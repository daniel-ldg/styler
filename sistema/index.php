<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Styler</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>

<body>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.4/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

    <?php

    session_start();

    include 'vista/navbar.php';

    include __DIR__ . '/controlador/estadoSuscripcion.php';
    include __DIR__ . '/controlador/tarjetasCliente.php';
    
    if (empty($_SESSION["usuarioId"])) {
        include 'vista/coverInicioSesion.php';
    } else if (!estaSuscrito($_SESSION["usuarioCustomerId"], "prod_K5wBvrqSW7jh1b")) {
        include 'vista/coverSuscripcion.php';
    } else if (!tieneTarjetas($_SESSION["usuarioCustomerId"])) {
        include 'vista/coverTarjetas.php';
    } else {
        include 'vista/contenido.php';
    }

    ?>

</body>

</html>