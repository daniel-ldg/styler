<div class="d-flex h-100 text-center mt-5">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

        <main class="px-3">
            <h1>No está autorizado a acceder a esta página.</h1>
            <p class="lead">No ha iniciado sesión, debe iniciar sesión para ver esta página.</p>
            <p class="lead">
                <a href="login.php" class="btn btn-lg btn-primary">Iniciar sesión</a>
            </p>
        </main>

    </div>

</div>