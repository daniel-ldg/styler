<div class="d-flex h-100 text-center mt-5">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

        <main class="px-3">
            <h1>No está autorizado a acceder a esta página.</h1>
            <p class="lead">No hay ningún método de pago asociado a tu cuenta.</p>
            <p class="lead">
                <a href="#" class="btn btn-lg btn-primary" id="agregar-metodo-pago">Agregar método de pago</a>
            </p>
        </main>

    </div>

</div>

<script>
    $("#agregar-metodo-pago").on("click", function() {
    $.ajax({
      url: "controlador/CrearPortalCliente.php",
      type: "POST",
      data: {
        'url': window.location.href,
      },
      success: function(respuesta) {
        window.location.href = respuesta.url
      },
      error: function() {
        alert("error")
      }
    })
  })
</script>