<?php

if (session_status() === PHP_SESSION_NONE) {
  session_start();
}

$logeado = isset($_SESSION["usuarioId"]);
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php"><i class="bi bi-scissors"></i> Styler</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Inicio</a>
        </li>

        <?php
        if ($logeado) {
          echo '
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Cuenta
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                  <li><a class="dropdown-item" href="#" id="btn-administrar">Adminsitrar suscripciones</a></li>
                  <li><a class="dropdown-item" href="#" id="cerrar_sesion">Cerrar sesión</a></li>
                </ul>
              </li>
            ';
        }
        ?>
      </ul>
      <div class="d-flex">
        <?php
        if ($logeado) {
          echo '<span>' . $_SESSION["usuarioEmail"] . '</span>';
        };
        ?>
      </div>
    </div>
  </div>
</nav>

<script>
  $("#cerrar_sesion").on("click", function() {
    $.ajax({
      url: "controlador/logout.php",
      type: "GET",
      success: function() {
        window.location.replace("index.php")
      },
      error: function() {
        Swal.fire({
          icon: 'error',
        })
      }
    })
  })

  $("#btn-administrar").on("click", function() {
    $.ajax({
      url: "controlador/CrearPortalCliente.php",
      type: "POST",
      data: {
        'url': window.location.href,
      },
      success: function(respuesta) {
        window.location.href = respuesta.url
      },
      error: function() {
        alert("error")
      }
    })
  })
</script>