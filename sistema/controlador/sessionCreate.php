<?php

include __DIR__ . '/Stripe.php';
include __DIR__ . '/validar.php';
include __DIR__ . '/existeEmail.php';

if (!existenDatos("email")) {
    http_response_code(400);
    exit;
}

$email = $_POST["email"];

if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    http_response_code(400);
    exit();
}

if (emailYaFueRegistrado($email)) {
    http_response_code(403);
    exit();
}

try {
    
    $session = $stripe->checkout->sessions->create([
        'success_url' => 'http://livsoftware.com.mx/stripe/sistema/login.php?newAcc=true',
        'cancel_url' => 'http://livsoftware.com.mx/stripe/landing/',
        'payment_method_types' => ['card'],
        'client_reference_id' => 'nueva_suscripcion_styler',
        'customer_email' => $email,
        'subscription_data' => [
            'trial_period_days' => 30
        ],
        'line_items' => [
            [
                'price' => 'price_1JRkJ5Ehw8eLdPcfPBkthRQf',
                'quantity' => 1,
            ],
        ],
        'mode' => 'subscription'
    ]);

    $respuesta = [
        "url" => $session->url
    ];

    header('Content-Type: application/json');
    echo json_encode($respuesta);
} catch (Error $e) {
    http_response_code(500);
}