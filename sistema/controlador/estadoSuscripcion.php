<?php

include __DIR__ . '/../modelo/modeloUsuarios.php';


function estaSuscrito($customerId, $productId) {
    $respuestaDb = ModeloUsuarios::tieneSuscripcionActiva($customerId, $productId);

    if (is_null($respuestaDb)) {
        // todo: manejar error
    } else {
        return $respuestaDb;
    }
}