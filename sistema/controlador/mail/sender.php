<?php

include_once 'security.php';
include __DIR__ . '/../../modelo/modeloEventos.php';

ignore_user_abort(true);
set_time_limit(0);

if (!isset($_POST["payload"]) || !isset($_POST["key"])) {
    http_response_code(400);
    exit();
}

$payload = $_POST["payload"];
$payloadKey = $_POST["key"];

$localKey = hash_string($payload);

if ($localKey != $payloadKey) {
    http_response_code(400);
    exit();
}

$decrypted = encrypt_decrypt_string("decrypt", $payload);

$unserialized = unserialize($decrypted);

// todo: con la variable $unserialized tomar el template y cambiar los placeholders y enviar el email

switch ($unserialized["tipo"]) {
    case 'nueva_cuenta':
        $template = file_get_contents("templates/nuevaCuenta.html");
        $subject = "Bienvenido a Styler";
        break;


    case 'nueva_tarjeta':
        $template = file_get_contents("templates/nuevaTarjeta.html");
        $subject = "Nueva tarjeta agregada";
        break;

    case 'nueva_tarjeta_error':
        $template = file_get_contents("templates/nuevaTarjetaError.html");
        $subject = "No fue posible guardar tu tarjeta";
        break;
        

    case 'tarjeta_eliminada':
        $template = file_get_contents("templates/tarjetaEliminada.html");
        $subject = "Se eliminó tu tarjeta";
        break;

    case 'inicio_prueba':
        $template = file_get_contents("templates/inicioPrueba.html");
        $subject = "Inició tu prueba gratuita";
        break;

    case 'fin_suscripcion':
        $template = file_get_contents("templates/finSuscripcion.html");
        $subject = "Suscripción finalizada";
        break;

    default:
        http_response_code(400);
        exit();
        break;
}

$vars = $unserialized["vars"];
foreach ($vars as $key => $value) {
    $template = str_replace('{{' . $key . '}}', $value, $template);
}

$exito = enviarHtml($unserialized["email"], $subject, $template);

if ($exito) {
    ModeloEventos::registrarAccion($unserialized["eventId"], false, false, "Se envió el correo: " . $unserialized["tipo"] . " a: " . $unserialized["email"]);
} else {
    ModeloEventos::registrarAccion($unserialized["eventId"], true, true, "Error al enviar el correo: " . $unserialized["tipo"] . " a: " . $unserialized["email"]);
}

function enviar($email, $titulo, $mensaje) {
    $from = "info@livsoftware.com.mx";
    $cabeceras = "From: <" . $from . ">\r\n";
    return mail($email, $titulo, $mensaje, $cabeceras);
}

function enviarHtml($email, $titulo, $mensaje) {
    $fromName = "Styler \xE2\x9C\x82";

    //caracteres especiales en nombre
    $fromName = '=?UTF-8?B?' . base64_encode($fromName) . '?=';

    $fromMail = "info@livsoftware.com.mx";

    // caracteres especiales en titulo
    $titulo = '=?UTF-8?B?' . base64_encode($titulo) . '?=';

    $cabeceras = "MIME-Version: 1.0" . "\r\n";
    $cabeceras .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $cabeceras .= "From: \"$fromName\" <$fromMail>\r\n";
    return mail($email, $titulo, $mensaje, $cabeceras);
}