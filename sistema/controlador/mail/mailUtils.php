<?php

include_once 'security.php';

function enviarNuevaCuenta($eventId, $email, $nombre, $password) {
    $vars = [
        "tipo" => "nueva_cuenta",
        "email" => $email,
        "eventId" => $eventId,
        "vars" => [
            "nombre" => $nombre,
            "email" => $email,
            "password" => $password
        ]
    ];

    asyncHttpRequest($vars);
}

function enviarNuevaTarjeta($eventId, $email, $nombre, $brand, $last4) {
    $vars = [
        "tipo" => "nueva_tarjeta",
        "email" => $email,
        "eventId" => $eventId,
        "vars" => [
            "nombre" => $nombre,
            "brand" => $brand,
            "last4" => $last4
        ]
    ];

    asyncHttpRequest($vars);
}

function enviarNuevaTarjetaError($eventId, $email, $nombre, $brand, $last4) {
    $vars = [
        "tipo" => "nueva_tarjeta_error",
        "email" => $email,
        "eventId" => $eventId,
        "vars" => [
            "nombre" => $nombre,
            "brand" => $brand,
            "last4" => $last4
        ]
    ];

    asyncHttpRequest($vars);
}

function enviarTarjetaEliminada($eventId, $email, $nombre, $brand, $last4) {
    $vars = [
        "tipo" => "tarjeta_eliminada",
        "email" => $email,
        "eventId" => $eventId,
        "vars" => [
            "nombre" => $nombre,
            "brand" => $brand,
            "last4" => $last4
        ]
    ];

    asyncHttpRequest($vars);
}

function enviarInicioPrueba($eventId, $email, $nombre, $fechaFin, $costo) {
    $vars = [
        "tipo" => "inicio_prueba",
        "email" => $email,
        "eventId" => $eventId,
        "vars" => [
            "nombre" => $nombre,
            "fechafin" => $fechaFin,
            "costo" => $costo
        ]
    ];

    asyncHttpRequest($vars);
}

function enviarFinSuscripcion($eventId, $email, $nombre) {
    $vars = [
        "tipo" => "fin_suscripcion",
        "email" => $email,
        "eventId" => $eventId,
        "vars" => [
            "nombre" => $nombre
        ]
    ];

    asyncHttpRequest($vars);
}

function asyncHttpRequest($vars) {
    $host = "livsoftware.com.mx";

    $encryptedPayload = encrypt_decrypt_string("encrypt", serialize($vars));
    $data = [
        "payload" => $encryptedPayload,
        "key" => hash_string($encryptedPayload)
    ];

    $fp = fsockopen($host, 80);
    if($fp) {
        $content = http_build_query($data);
        fwrite($fp, "POST /stripe/sistema/controlador/mail/sender.php HTTP/1.1\r\n");
        fwrite($fp, "Host: $host\r\n");
        fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
        fwrite($fp, "Content-Length: " .strlen($content) . "\r\n");
        fwrite($fp, "Connection: close\r\n");
        fwrite($fp, "\r\n");

        fwrite($fp, $content);

        /*header('Content-type: text/plain');
        while (!feof($fp)) {
            echo fgets($fp, 1024);
        }*/

        fclose($fp);
    }
    
}