<?php

/**
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 * 
 * from: https://stackoverflow.com/questions/23889885/php-how-encode-decode-text-with-key/23889971
 * 
 * @param string $action: can be 'encrypt' or 'decrypt'
 * @param string $string: string to encrypt or decrypt
 *
 * @return string
 */
function encrypt_decrypt_string($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'aQSdrJA3DQSWVPThPvQvHY2MXAfZ3fM3';
    $secret_iv = 'JSkhm9NWzgcJ7yGhRGYj2aDGZTxRK3Xk';
    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function hash_string($string) {
    $secret = "9MqZTYDvSKwV4W3c4pZ4LzYWRQGEVZ5q";
    
    $hash = hash_hmac("ripemd160", $string, $secret);

    return $hash;
}