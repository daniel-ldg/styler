<?php

use Stripe\Subscription;

include __DIR__ . '/Stripe.php';
include __DIR__ . '/crearUsuario.php';
include __DIR__ . '/../modelo/modeloTarjetas.php';

// You can find your endpoint's secret in your webhook settings
$endpoint_secret = 'whsec_mUZmS3C4JumNupcENVE9HsJPm0i9Qs9G';

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
    $event = \Stripe\Webhook::constructEvent(
        $payload,
        $sig_header,
        $endpoint_secret
    );
} catch (\UnexpectedValueException $e) {
    // Invalid payload
    http_response_code(400);
    exit();
} catch (\Stripe\Exception\SignatureVerificationException $e) {
    // Invalid signature
    http_response_code(400);
    exit();
}

switch ($event->type) {

    case 'checkout.session.completed':
        // Payment is successful and the subscription is created.
        // You should provision the subscription and save the customer ID to your database.

        // procesar evento con client_reference_id = styler_subscription
        // y guardar en base de datos el id del customer recien creado
        // necesario para procesar despues el evento payment_method.attached 
        // y asi diferenciar entre customers creados para styler y customers
        // creados para otro producto (mentor, baul, etc...) 
        // aqui se debe hacer tambien una llamada a la api de stripe
        // $stripe->paymentMethods->all para hacer un retrive de los metodos de pago
        // relacionados al id del customer
        // por si este evento llega despues del evento payment_method.attached
        
        $session = $event->data->object;

        break;

    case 'invoice.payment_failed':
        // Sent each billing interval if there is an issue with your customer’s payment method.

        break;

    case 'invoice.paid':
        // Sent each billing interval when a payment succeeds.

        break;

    case 'payment_method.attached':
        // guardar huella dactilar de tarjeta en la base de datos
        $paymentMethod = $event->data->object;

        $respuestaBd = ModeloTarjetas::attach($paymentMethod->card->fingerprint, $paymentMethod->customer);

        if ($respuestaBd) {
            error_log("Tarjeta '" . $paymentMethod->card->fingerprint . "' guardada en bd");
        } else {
            error_log("Tarjeta '" . $paymentMethod->card->fingerprint . "' no guardada en bd. Quitando tarjeta de cliente en Stripe...");

            $stripe->paymentMethods->detach(
                $paymentMethod->id,
                []
            );
        }

        break;

    case 'payment_method.detached':
        // si el cliente quitó una tarjeta desde el Customer Portal hay que quitarla de la base de datos
        $paymentMethod = $event->data->object;

        $respuestaBd = ModeloTarjetas::detach($paymentMethod->card->fingerprint, $paymentMethod->customer);

        if ($respuestaBd) {
            error_log("Tarjeta '" . $paymentMethod->card->fingerprint . "' quitada de bd");
        } else {
            error_log("Tarjeta '" . $paymentMethod->card->fingerprint . "' no quitada de bd.");
        }
        
        break;

    case 'customer.subscription.created':
        $eventSubscription = $event->data->object;

        $subscription = $stripe->subscriptions->retrieve(
            $eventSubscription->id,
            []
        );

        $product = $subscription->items->data[0]->price->product;

        if ($product != 'prod_K5wBvrqSW7jh1b') {
            return;
        }

        $customer = $stripe->customers->retrieve(
            $subscription->customer,
            []
        );

        $status = $subscription->status;

        if ($status == 'trialing') {
            error_log("Se suscribió un nuevo cliente e inicio una prueba gratuita:");
            error_log("Generando cuenta nueva...");
            error_log("email: " . $customer->email);
            error_log("customerId: " . $customer->id);
            error_log("subscriptionId: " . $subscription->id);
            $contraseñaGenerada = crearCuenta(
                $customer->email,
                $customer->id,
                $subscription->id,
                $status
            );
            if (!is_null($contraseñaGenerada)) {
                error_log("Cuenta generada con exito. Contraseña: " . $contraseñaGenerada);
            } else {
                error_log("Cuenta no generada. Borrando de stripe...");
                $stripe->customers->delete(
                  $customer->id,
                  []
                );
            }
        }
        break;

    case 'customer.subscription.updated':
    case 'customer.subscription.deleted':
        $eventSubscription = $event->data->object;

        $subscription = $stripe->subscriptions->retrieve(
            $eventSubscription->id,
            []
        );

        $product = $subscription->items->data[0]->price->product;

        if ($product != 'prod_K5wBvrqSW7jh1b') {
            return;
        }

        $customer = $stripe->customers->retrieve(
            $subscription->customer,
            []
        );

        $resultado = ModeloUsuarios::actualizarEstadoSuscripcion($subscription->customer, $subscription->status);

        if ($resultado) {
            error_log("La suscripcion $subscription->id[$subscription->customer] cambió a estado: $subscription->status");
        } else {
            error_log("error al actualizar estado subscripción");
        }

        break;
}
