<?php

// Stripe webhook event handler
// Version 3 (21 09 21) @daniel-ldg
// Suscripciones (Nuevos usuarios)

include __DIR__ . '/Stripe.php';
include __DIR__ . '/crearUsuario.php';
include __DIR__ . '/../modelo/modeloTarjetas.php';
include __DIR__ . '/../modelo/modeloEventos.php';
include __DIR__ . '/mail/mailUtils.php';

// Secreto de firma (Se encuentra en el dashboard de stripe)
$endpoint_secret = 'whsec_VZZjPdKE7HQWTwRKea6KrPxVPJLkkr6E';

// Referencia de checkout para este webhook
$client_reference_id = 'nueva_suscripcion_styler';

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
	$event = \Stripe\Webhook::constructEvent(
		$payload,
		$sig_header,
		$endpoint_secret
	);
} catch (\UnexpectedValueException $e) {
	// Payload no válido
	http_response_code(400);
	exit();
} catch (\Stripe\Exception\SignatureVerificationException $e) {
	// Secreto de firma no válido
	http_response_code(400);
	exit();
}

$eventId = $event->id;
$eventType = $event->type;

if (eventoManejadoConExito($eventId)) {
	http_response_code(202);
	exit();
} else {
	$exito = registrarEvento($eventId, $eventType);

	if (!$exito) {
		http_response_code(500);
		exit();
	}
}

// Manejar eventos
switch ($eventType) {

		// Se maneja este evento para guardar en la base de datos el id del customer recien creado en el checkout
		//
		// Solo se guardan los usuarios que usen un checout session con client_reference_id == $client_reference_id
	case 'checkout.session.completed':
		$checkout = $event->data->object;


		// checar si es el client_reference_id
		// checar si no tiene cuenta en el sistema

		// retrive subscription object
		// retrive customer object
		// retrive payment methods de customer
		
		// crear cuenta en el sistema
		// crear registro inicio de suscripcion


		if ($checkout->client_reference_id != $client_reference_id) {
			// todo: exit
		}
		
		if (tieneCuenta($checkout->customer)) {
			// todo: exit
	  	}

		// crear cuenta en el sistema

		$customer = $stripe->customers->retrieve(
			$checkout->customer,
			[]
	  	);

	  	$customerId = $customer->id;
		
		$contraseñaCreada = crearCuenta($customerId, $customer->email, $customer->name);

		if ($contraseñaCreada) {
		  	emailCuentaCreada($eventId, $customerId, $contraseñaCreada);
			registrarAccionEvento($eventId, false, false, "Se creó una cuenta en el sistema para el customer [$customerId]");
		} else {
			try {
				$stripe->customers->delete(
					$customerId,
					[]
				);
				registrarAccionEvento($eventId, true, false, "Ocurrió un error y no se pudo crear una cuenta en el sistema para el customer [$customerId]. Se borro el customer en Stripe");
			} catch (Exception $e) {
				registrarAccionEvento(
					$eventId,
					true,
					true,
					"Ocurrió un error y no se pudo crear una cuenta en el sistema para el customer [$customerId]. No se borró en Stripe: " . $e->getMessage()
				);
				finalizarError($eventId, 500);
			}
		}

		// crear registro de suscripcion

		$subscription = $stripe->subscriptions->retrieve(
			$checkout->subscription,
			[]
	  	);

	  	$subscriptionId = $subscription->id;
		$productId = $subscription->items->data[0]->price->product;
		$productAmount = $subscription->items->data[0]->price->unit_amount_decimal;
		$status = $subscription->status;

		$exito = registrarInicioDeSuscripcion($subscriptionId, $customerId, $productId, $status);

		if ($exito) {
			if ($status == 'trialing') {
				emailInicioDePrueba($eventId, $customerId, $productId, $subscription->trial_end, $productAmount);
				registrarAccionEvento($eventId, false, false, "El customer [$customerId] inició un periodo de prueba del producto [$productId]");
			} else if ($status == 'active') {
				registrarAccionEvento($eventId, false, false, "El customer [$customerId] inició una suscripción al producto [$productId]");
			}
		} else {
			if ($status == 'trialing') {
				registrarAccionEvento($eventId, true, true, "El customer [$customerId] inició un periodo de prueba del producto [$productId] pero ocurrió un error al guardar en la base de datos");
			} else if ($status == 'active') {
				registrarAccionEvento($eventId, true, true, "El customer [$customerId] inició una suscripción al producto [$productId] pero ocurrió un error al guardar en la base de datos");
			}
			finalizarError($eventId, 500);
		}

		// manejar tarjetas de cliente

		$paymentMethods = $stripe->paymentMethods->all([
			'customer' => $checkout->customer,
			'type' => 'card',
		]);

		foreach($paymentMethods->data as $paymentMethod) {
			manejarPaymentMethodAttached($stripe, $eventId, $paymentMethod);
		}

		finalizarOk($eventId);

		break;

		// Se maneja este evento para registrar en la base de datos de las tarjetas que asocian los usuarios a sus cuentas
		//
		// Antes de guardar la tarjeta en la base de datos se válida que la tarjeta no exista en la base de datos
		//
		// Si la tarjeta ya existe en la base de datos (un usuario ya la tiene asociada) NO se guarda otra vez y 
		// se le borra la tarjeta de Stripe al segundo usuario (esto evita diferentes usuarios usen la misma tarjeta)
		//
		// Este evento solo se ejecuta si el usuario ya esta registrado en la base de datos (los usuaruis se registran
		// en el evento checkout.session.completed)
	case 'payment_method.attached':
		$paymentMethod = $event->data->object;

		manejarPaymentMethodAttached($stripe, $eventId, $paymentMethod);

		finalizarOk($eventId);

		break;


		// Se maneja este evento para borrar una tarjeta de la base de datos cuando un usuario borra una tarjeta de su cuenta
		// por ejemplo desde el portal de clientes de Stripe
		//
		// La tarjeta se borra solo si el valor customer y fingerprint del objeto PaymentMethod estan en la base de datos
	case 'payment_method.detached':
		$paymentMethod = $event->data->object;

		if ($paymentMethod->type == "card") {
			$customerId = $paymentMethod->customer;
			if (is_null($customerId)) {
				$customerId = $event->data->previous_attributes->customer;
			}
			$fingerprint = $paymentMethod->card->fingerprint;
			$brand = $paymentMethod->card->brand;
			$last4 = $paymentMethod->card->last4;
			if (usuarioTieneTarjeta($customerId, $fingerprint)) {
				$exito = borrarTarjeta($customerId, $fingerprint);
				if ($exito) {
					emailTarjetaBorrada($eventId, $customerId, $brand, $last4);
					registrarAccionEvento(
						$eventId,
						false,
						false,
						"Se borró la tarjeta $brand, $last4 [$fingerprint] de customer [$customerId] con éxito."
					);
					finalizarOk($eventId);
				} else {
					registrarAccionEvento(
						$eventId,
						true,
						true,
						"No se borró la tarjeta $brand, $last4 [$fingerprint] de customer [$customerId]. Ocurrió un error al borrar de la base de datos."
					);
					finalizarError($eventId, 500);
				}
			} else {
				registrarAccionEvento(
					$eventId,
					true,
					false,
					"No se borró la tarjeta $brand, $last4 [$fingerprint] de customer [$customerId]. No se encontró en la base de datos."
				);
				finalizarOk($eventId);
			}
		}

		break;

	case 'customer.subscription.updated':
		$subscription = $event->data->object;
		$subscriptionId = $subscription->id;
		$customerId = $subscription->customer;
		$status = $subscription->status;
		// Actualmente solo soporta 1 producto por suscripcion
		$productId = $subscription->items->data[0]->price->product;

		$exito = registrarCambioSuscripcion($subscriptionId, $status);
		if ($exito) {
			registrarAccionEvento($eventId, false, false, "El customer [$customerId] cambió el estado de una suscripción al producto [$productId]: $status");
			finalizarOk($eventId);
		} else {
			registrarAccionEvento($eventId, true, true, "El customer [$customerId] cambió el estado de una suscripción al producto [$productId]: $status. Pero ocurrió un error al guardar en la base de datos");
			finalizarError($eventId, 500);
		}

		break;

	case 'customer.subscription.deleted':
		$subscription = $event->data->object;
		$subscriptionId = $subscription->id;
		$customerId = $subscription->customer;
		$status = $subscription->status;
		// Actualmente solo soporta 1 producto por suscripcion
		$productId = $subscription->items->data[0]->price->product;

		$exito = registrarCambioSuscripcion($subscriptionId, $status);
		if ($exito) {
			emailFinDeSuscripcion($eventId, $customerId, $productId);
			registrarAccionEvento($eventId, false, false, "El customer [$customerId] finalizó una suscripción al producto [$productId]");
			finalizarOk($eventId);
		} else {
			registrarAccionEvento($eventId, true, true, "El customer [$customerId] finalizó una suscripción al producto [$productId] pero ocurrió un error al guardar en la base de datos");
			finalizarError($eventId, 500);
		}

		break;

		// ... handle other event types
	default:
		echo 'Received unknown event type ' . $event->type;
}

registrarEventoOk($eventId);
http_response_code(203);
exit();

function manejarPaymentMethodAttached($stripe, $eventId, $paymentMethod) {
	if ($paymentMethod->type == "card") {
		$customerId = $paymentMethod->customer;
		if (tieneCuenta($customerId)) {
			$fingerprint = $paymentMethod->card->fingerprint;
			$brand = $paymentMethod->card->brand;
			$last4 = $paymentMethod->card->last4;

			if (existeTarjeta($fingerprint)) {
				if (!usuarioTieneTarjeta($customerId, $fingerprint)) {
					try {
						$stripe->paymentMethods->detach(
							$paymentMethod->id,
							[]
						);
						emailNuevaTarjetaError($eventId, $customerId, $brand, $last4);
						registrarAccionEvento(
							$eventId,
							true,
							false,
							"No se guardó la tarjeta $brand $last4 [$fingerprint] de customer [$customerId]. Ya existe esa tarjeta en la base de datos. Se borró en Stripe."
						);
					} catch (Exception $e) {
						registrarAccionEvento(
							$eventId,
							true,
							true,
							"No se guardó la tarjeta $brand $last4 [$fingerprint] de customer [$customerId]. Ya existe esa tarjeta en la base de datos. No se borró en Stripe: " . $e->getMessage()
						);
						finalizarError($eventId, 500);
					}
				} else {
					// ignorar (es una tarjeta repetida pero del mismo usuario)
				}
			} else {
				$exito = guardarTarjeta($customerId, $fingerprint, $brand, $last4);
				if ($exito) {
					emailNuevaTarjeta($eventId, $customerId, $brand, $last4);
					registrarAccionEvento(
						$eventId,
						false,
						false,
						"Se guardó la tarjeta $brand $last4 [$fingerprint] de customer [$customerId] con éxito."
					);
				} else {
					try {
						$stripe->paymentMethods->detach(
							$paymentMethod->id,
							[]
						);
						emailNuevaTarjetaError($eventId, $customerId, $brand, $last4);
						registrarAccionEvento(
							$eventId,
							true,
							false,
							"No se guardó la tarjeta $brand $last4 [$fingerprint] de customer [$customerId]. Ocurrió un error al guardar en base de datos. Se borró en Stripe."
						);
					} catch (Exception $e) {
						emailNuevaTarjetaError($eventId, $customerId, $brand, $last4);
						registrarAccionEvento(
							$eventId,
							true,
							true,
							"No se guardó la tarjeta $brand $last4 [$fingerprint] de customer [$customerId]. Ocurrió un error al guardar en base de datos. No se borró en Stripe: " . $e->getMessage()
						);
						finalizarError($eventId, 500);
					}
				}
			}
		}
	}
}

function finalizarOk($eventId) {
	registrarEventoOk($eventId);
	http_response_code(200);
	exit();
}

function finalizarError($eventId, $code) {
	http_response_code($code);
	exit();
}


// Funciones de email:
// 
// El propósito de estas funciones es informar al usuario sobre las acciones tomadas

// Se le informa al usuario de una nueva tarjeta asosciada a su cuenta 
function emailNuevaTarjeta($eventId, $customerId, $brand, $last4) {
	$customer = ModeloUsuarios::getByCustomerId($customerId);
	enviarNuevaTarjeta($eventId, $customer["email"], $customer["nombre"], $brand, $last4);
}

// Se le informa al usuario que la tarjeta que registró no puede ser usada y se borró de su cuenta (Stripe)
function emailNuevaTarjetaError($eventId, $customerId, $brand, $last4) {
	$customer = ModeloUsuarios::getByCustomerId($customerId);
	enviarNuevaTarjetaError($eventId, $customer["email"], $customer["nombre"], $brand, $last4);
}

// Se le informa al usuario que se borró una tarjeta asociada a su cuenta (Por accion del usuario no por algún error)
function emailTarjetaBorrada($eventId, $customerId, $brand, $last4) {
	$customer = ModeloUsuarios::getByCustomerId($customerId);
	enviarTarjetaEliminada($eventId, $customer["email"], $customer["nombre"], $brand, $last4);
}

// Se le informa al usuario que se creo (aquí puede enviar la contraseña de acceso)
function emailCuentaCreada($eventId, $customerId, $contraseñaCreada) {
	$customer = ModeloUsuarios::getByCustomerId($customerId);
	enviarNuevaCuenta($eventId, $customer["email"], $customer["nombre"], $contraseñaCreada);
}

// Se le informa el usuario que su suscripcion (modo prueba gratuita) ha comenzado
function emailInicioDePrueba($eventId, $customerId, $productId, $fechaFin, $costo) {
	if ($productId == "prod_K5wBvrqSW7jh1b") {
		$customer = ModeloUsuarios::getByCustomerId($customerId);
		setlocale(LC_TIME, "es_MX");
		$fechaFinFormato = strftime("%e de %B de %G", $fechaFin);
		$costoFormato = "$" . substr($costo, 0, strlen($costo) - 2) . "." . substr($costo, -2);
		enviarInicioPrueba($eventId, $customer["email"], $customer["nombre"], $fechaFinFormato, $costoFormato);
	}
}

// Se le informa el usuario que su suscripcion ha terminado
function emailFinDeSuscripcion($eventId, $customerId, $productId) {
	if ($productId == "prod_K5wBvrqSW7jh1b") {
		$customer = ModeloUsuarios::getByCustomerId($customerId);
		enviarFinSuscripcion($eventId, $customer["email"], $customer["nombre"]);
	}
}

// Funciones de eventos
// 
// El propósito de estas funciones es registrar en la base de datos los eventos recibidos y las acciones que se tomaron para:
// - Tener un registro con los eventos recibidos
// - Tener un registro con las acciones que tomo el sistema
// - Tener un registro con los errores
// - No procesar el mismo evento 2 veces

// Debe regresar true si el evento ya fue manejado con exito
function eventoManejadoConExito($eventId) {
	return ModeloEventos::eventoManejadoConExito($eventId);
}

// Debe crear un nuevo registro en la base de datos con el id = $eventId y tipo = $eventType
// Debe regresar true si creo el registro con exito, false si no
function registrarEvento($eventId, $eventType) {
	return ModeloEventos::registrarEvento($eventId, $eventType);
}

// Debe crear un nuevo registro en la base de datos con los datos del evento y accion del sistema
// El un evento puede registrar multiples acciones 
function registrarAccionEvento($eventId, $error, $critico, $action) {
	ModeloEventos::registrarAccion($eventId, $error, $critico, $action);
}

// Debe crear o midificar un registro con el id del evento y marcarlo como manejado con exito
function registrarEventoOk($eventId) {
	ModeloEventos::marcarEventoExitoso($eventId, true);
}

// Funciones de tarjetas
//
// El propósito de estas funciones es administrar los registros de las tarjetas en la base de datos

// Debe regresar true si existe un registro en la base de datos con el custromerId y fingerprint o false si no
function usuarioTieneTarjeta($customerId, $fingerprint) {
	return ModeloTarjetas::tieneTarjeta($customerId, $fingerprint);
}

// Debe borrar el registro con el custromerId y fingerprint.
// Debe regresar true si borró el registro con exito, false si no
function borrarTarjeta($customerId, $fingerprint) {
	return ModeloTarjetas::detach($fingerprint, $customerId);
}

// Debe regresar true si existe alguna tarjeta en la base de datos con el fingerprint
function existeTarjeta($fingerprint) {
	return ModeloTarjetas::existe($fingerprint);
}

// Debe guardar un nuevo registro con los datos de la tarjeta
// Debe regresar true si guardó el registro con exito, false si no
function guardarTarjeta($customerId, $fingerprint, $brand, $last4) {
	return ModeloTarjetas::attach($fingerprint, $customerId, $brand, $last4);
}

// Funciones de Cuentas
// 
// Estas funciones interactuan con el sistema para administrar el acceso del usuario

// Debe regresar true si ya existe una cuenta en el sistema relacionada con el id del customer
function tieneCuenta($customerId) {
	return ModeloUsuarios::tieneCuenta($customerId);
}

// Debe crear una cuenta en el sistema
// Debe regresar una cadena de texto con la contraseña creada si la cuenta fue creada con exito 
// Debe regresar null si no fue posible crear la cuenta
function crearCuenta($customerId, $email, $nombre) {

	$password = crearCuentaEnSistema(
		$email,
		$customerId,
		$nombre
	);

	// todo: enviar email con contrasena
	error_log("$email : $password");

	return $password;
}

// Debe registrar un nuevo registro de suscripcion
// Debe regresar true si guardó el registro con éxito, false si no
function registrarInicioDeSuscripcion($subscriptionId, $customerId, $productId, $status) {
	return ModeloUsuarios::registrarSuscripcion($subscriptionId, $customerId, $productId, $status);
}

// Debe registrar el cambio de una suscripcion
// Debe regresar true si guardó el registro con éxito, false si no
function registrarCambioSuscripcion($subscriptionId, $status) {
	return ModeloUsuarios::cambiarEstadoSuscripcion($subscriptionId, $status);
}