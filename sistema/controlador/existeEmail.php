<?php

include __DIR__ . '/../modelo/modeloUsuarios.php';

function emailYaFueRegistrado($email) {
    $respuestaDb = ModeloUsuarios::existeEmail($email);

    if (is_null($respuestaDb)) {
        // todo: manejar error
    } else {
        return $respuestaDb;
    }
}