<?php

include __DIR__ . '/validar.php';
include __DIR__ . '/../modelo/modeloUsuarios.php';
include __DIR__ . '/Stripe.php';

session_start();

if (!isset($_SESSION["usuarioCustomerId"]) || !existenDatos("url")) {
    http_response_code(400);
    exit;
}

try {
    $customerId = $_SESSION["usuarioCustomerId"];
    $url = $_POST["url"];

    $portal = $stripe->billingPortal->sessions->create([
        'customer' => $customerId,
        'return_url' => $url
    ]);

    $respuesta = [
        "url" => $portal["url"]
    ];

    header('Content-Type: application/json');
    echo json_encode($respuesta);
    
} catch (Error $e) {
    http_response_code(500);
}
