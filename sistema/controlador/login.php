<?php

include __DIR__ . '/validar.php';
include __DIR__ . '/../modelo/modeloUsuarios.php';

header('Content-Type: application/json');

if (!existenDatos("email", "password")) {
    http_response_code(400);
    exit;
}

$email = $_POST["email"];
$contraseña = $_POST["password"];

$usuario = ModeloUsuarios::getByEmail($email);

if ($usuario == null) {
    http_response_code(404);
    exit;
}

if (password_verify($contraseña, $usuario["password_hash"])) {
    session_set_cookie_params(0, '/stripe-styler/sistema');
    session_start();
    $_SESSION["usuarioId"] = $usuario["id"];
    $_SESSION["usuarioEmail"] = $usuario["email"];
    $_SESSION["usuarioCustomerId"] = $usuario["customer_id"];

    echo json_encode(["status" => "ok"]);
} else {
    http_response_code(401);
    exit;
}
