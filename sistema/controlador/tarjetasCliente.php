<?php

include __DIR__ . '/../modelo/modeloTarjetas.php';


function tieneTarjetas($customerId) {
    $respuestaDb = ModeloTarjetas::numTarjetas($customerId);

    if (is_null($respuestaDb)) {
        // todo: manejar error
    } else {
        return $respuestaDb > 0;
    }
}