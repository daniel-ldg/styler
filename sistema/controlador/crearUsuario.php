<?php

include __DIR__ . '/validar.php';
include __DIR__ . '/../modelo/modeloUsuarios.php';

function crearCuentaEnSistema($email, $customerId, $nombre) {

    $password = generateRandomString(16);
    $password_hash = password_hash($password, PASSWORD_BCRYPT);

    $exitoGuardar = ModeloUsuarios::registrar($email, $password_hash, $customerId, $nombre);

    if ($exitoGuardar) {
        return $password;
    } else {
        return null;
    }
}