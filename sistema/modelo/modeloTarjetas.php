<?php

include_once 'conexion.php';

class ModeloTarjetas {

    private static $TABLA_CUSTOMERS = "customer";
    private static $TABLA_TARJETAS = "tarjeta";

    public static function attach($fingerprint, $customerId, $brand, $last4) {
        $tabla = self::$TABLA_TARJETAS;
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (fingerprint,customer_id,brand,last4) VALUES (:fingerprint,:customer_id,:brand,:last4)");
        $stmt->bindParam(":fingerprint", $fingerprint, PDO::PARAM_STR);
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);
        $stmt->bindParam(":brand", $brand, PDO::PARAM_STR);
        $stmt->bindParam(":last4", $last4, PDO::PARAM_STR);
        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function detach($fingerprint, $customerId) {
        $tabla = self::$TABLA_TARJETAS;
        $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE fingerprint = :fingerprint AND customer_id = :customer_id");
        $stmt->bindParam(":fingerprint", $fingerprint, PDO::PARAM_STR);
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function existe($fingerprint) {
        $tabla = self::$TABLA_TARJETAS;
        $stmt = Conexion::conectar()->prepare("SELECT EXISTS ( SELECT 1 FROM $tabla WHERE fingerprint = :fingerprint LIMIT 1) AS existe");
        $stmt->bindParam(":fingerprint", $fingerprint, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return boolval($fetch["existe"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }

    public static function tieneTarjeta($customerId, $fingerprint) {
        $tabla = self::$TABLA_TARJETAS;
        $stmt = Conexion::conectar()->prepare("SELECT EXISTS ( SELECT 1 FROM $tabla WHERE fingerprint = :fingerprint AND customer_id = :customer_id LIMIT 1) AS existe");
        $stmt->bindParam(":fingerprint", $fingerprint, PDO::PARAM_STR);
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return boolval($fetch["existe"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }

    public static function numTarjetas($customerId) {
        $tabla = self::$TABLA_TARJETAS;
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(1) AS num FROM $tabla WHERE customer_id = :customer_id");
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return intval($fetch["num"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }

}