<?php

class Conexion {

    public static function conectar() {

        $host = "159.89.42.93";
        $port = 3306;
        $dbName = "styler-stripe";
        $user = "styler";
        $password = "styler";
        $options = [
            PDO::MYSQL_ATTR_FOUND_ROWS => true
        ];

        return new PDO("mysql:host=$host;port=$port;dbname=$dbName", $user, $password, $options);
    }
}
