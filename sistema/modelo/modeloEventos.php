<?php

include_once 'conexion.php';

class ModeloEventos {

    private static $TABLA_EVENTOS = "evento";
    private static $TABLA_ACCIONES = "accion";

    public static function registrarEvento($id, $tipo) {
        $tabla = self::$TABLA_EVENTOS;
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id,tipo) VALUES (:id,:tipo)");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->bindParam(":tipo", $tipo, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function eventoManejadoConExito($id) {
        $tabla = self::$TABLA_EVENTOS;
        $stmt = Conexion::conectar()->prepare("SELECT EXISTS ( SELECT 1 FROM $tabla WHERE id = :id AND exito = 1 LIMIT 1) AS existe");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return boolval($fetch["existe"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null; 
    }

    public static function registrarAccion($idEvento, $error, $critico, $descripcion) {
        $tabla = self::$TABLA_ACCIONES;
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (idEvento,error,critico,descripcion) VALUES (:idEvento,:error,:critico,:descripcion)");
        $stmt->bindParam(":idEvento", $idEvento, PDO::PARAM_STR);
        $stmt->bindParam(":error", $error, PDO::PARAM_BOOL);
        $stmt->bindParam(":critico", $critico, PDO::PARAM_BOOL);
        $stmt->bindParam(":descripcion", $descripcion, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function marcarEventoExitoso($id, $exito) {
        $tabla = self::$TABLA_EVENTOS;
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET exito = :exito WHERE id = :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->bindParam(":exito", $exito, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }
}