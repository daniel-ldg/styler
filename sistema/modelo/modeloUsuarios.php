<?php

include_once 'conexion.php';

class ModeloUsuarios {

    private static $TABLA_USUARIOS = "usuario";
    private static $TABLA_SUSCRIPCIONES = "suscripcion";

    public static function tieneCuenta($customerId) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare("SELECT EXISTS ( SELECT 1 FROM $tabla WHERE customer_id = :id LIMIT 1) AS existe");
        $stmt->bindParam(":id", $customerId, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return boolval($fetch["existe"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }
    
    public static function registrar($email, $password, $customerId, $nombre) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (email,password_hash,customer_id,nombre) VALUES (:email,:password_hash,:customer_id,:nombre)");
        $stmt->bindParam(":email", $email, PDO::PARAM_STR);
        $stmt->bindParam(":password_hash", $password, PDO::PARAM_STR);
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);
        $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function registrarSuscripcion($id, $customerId, $productId, $estado) {
        $tabla = self::$TABLA_SUSCRIPCIONES;
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id,customer_id,product_id,estado) VALUES (:id,:customer_id,:product_id,:estado)");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);
        $stmt->bindParam(":product_id", $productId, PDO::PARAM_STR);
        $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function cambiarEstadoSuscripcion($id, $estado) {
        $tabla = self::$TABLA_SUSCRIPCIONES;
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = :estado WHERE id = :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return true;
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return false;
    }

    public static function existeEmail($email) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare("SELECT EXISTS ( SELECT 1 FROM $tabla WHERE email = :email LIMIT 1) AS existe");
        $stmt->bindParam(":email", $email, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return boolval($fetch["existe"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }

    public static function obtenerEmail($customerId) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare("SELECT email FROM $tabla WHERE customer_id = :customer_id LIMIT 1");
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return $fetch["email"];
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }

    public static function getByCustomerId($customerId) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE customer_id = :customer_id LIMIT 1");
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);
        
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                return $stmt->fetch(PDO::FETCH_ASSOC);
            }
        } else {
            error_log("bd_error:" . implode(":", $stmt->errorInfo()));
        }
        return null;
    }

    public static function getByEmail($email) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE email = :email LIMIT 1");
        $stmt->bindParam(":email", $email, PDO::PARAM_STR);
        
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                return $stmt->fetch(PDO::FETCH_ASSOC);
            }
        } else {
            error_log("bd_error:" . implode(":", $stmt->errorInfo()));
        }
        return null;
    }

    public static function tieneSuscripcionActiva($customerId, $productId) {
        $tabla = self::$TABLA_USUARIOS;
        $stmt = Conexion::conectar()->prepare(
            "SELECT EXISTS (SELECT * FROM usuario INNER JOIN suscripcion ON usuario.customer_id = suscripcion.customer_id 
            WHERE product_id = :product_id AND usuario.customer_id = :customer_id AND (estado = 'trialing' OR estado = 'active')) AS existe");
        $stmt->bindParam(":customer_id", $customerId, PDO::PARAM_STR);
        $stmt->bindParam(":product_id", $productId, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
            return boolval($fetch["existe"]);
        } else {
            error_log(implode(":", $stmt->errorInfo()));
        }
        return null;
    }
}
