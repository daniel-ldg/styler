$("#inicio-prueba").submit(function(e) {
    e.preventDefault()
    email = $(this).find("#email-prueba").val()

    btn = $(this).find("#btn-prueba")
    btn.html('Cargando...')
    btn.prop("disabled", true);
    btn.css('cursor', 'wait')

    $.ajax({
        url: "../sistema/controlador/sessionCreate.php",
        type: "POST",
        data: {
            'email': email,
        },
        success: function(respuesta) {
            window.location.href = respuesta.url
        },
        error: function() {
            btn.html('Iniciar prueba')
            btn.prop("disabled", false);
            btn.css('cursor', 'pointer')
        },
        statusCode: {
            403: function() {
              alert( "Error: El email ya fue registrado");
            },
            400: function() {
                alert( "Error: El email no es válido");
            }
          }
    })
})